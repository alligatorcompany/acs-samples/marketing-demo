{% if target.name == 'doc' %} 

with 
query_00 as (
  select * from {{ ref('H_COMMUNICATION') }} 
),
query_01 as (
  select * from {{ ref('H_RECIPIENT') }} 
),
query_02 as (
  select * from {{ ref('S_COMMUNICATION_S_VEEAM_R_HR_COMMUNICATION_U_VEEAM_SRC') }} 
),

bo_set as (
  select

  query_00.COMMUNICATION_BK as 'BK_COMMUNICATION',
  query_00.COMMUNICATION_H as 'HK_COMMUNICATION',

  query_01.RECIPIENT_H as 'HK_RECIPIENT',
  query_01.RECIPIENT_BK as 'BK_RECIPIENT',

  query_02.ACTIVITY_ID as 'ACTIVITY_ID',
  query_02.LEAD_ID as 'LEAD_ID',
  query_02.ACTIVITY_DATE as 'ACTIVITY_DATE',
  query_02.PRIMARY_ATTRIBUTE_VALUE_ID as 'PRIMARY_ATTRIBUTE_VALUE_ID',
  query_02.ACTIVITY_TYPE_ID as 'ACTIVITY_TYPE_ID',
  query_02.NAME as 'NAME',
  query_02.SENT as 'SENT',
  query_02.DELIVERED as 'DELIVERED',
  query_02.OPENED as 'OPENED',
  query_02.CKLICKED as 'CKLICKED',
  query_02.BOUNCE as 'BOUNCE',
  query_02.UNSUBSCRIBE as 'UNSUBSCRIBE',
  query_02.BOUNCE_SOFT as 'BOUNCE_SOFT'
  from 
   query_00 
    left join  query_02  on query_02.COMMUNICATION_H = query_00.COMMUNICATION_H
    left join {{ref('L_RECIPIENT_T_COMMUNICATION')}}  query_01_01  on query_01_01.COMMUNICATION_H = query_00.COMMUNICATION_H
    left join  query_01  on query_01.RECIPIENT_H = query_01_01.RECIPIENT_H
    )

select * from bo_set
{%else%}
select * from
{{ source('dv','COMMUNICATION_S_DVB_HUB') }}
{% endif %}
