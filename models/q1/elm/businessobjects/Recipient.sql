{% if target.name == 'doc' %} 

with 
query_00 as (
  select * from {{ ref('H_RECIPIENT') }} 
),
query_01 as (
  select * from {{ ref('S_RECIPIENT_S_VEEAM_R_HR_RECIPIENT_U_VEEAM_SRC_F_COUNTRY') }} 
),
query_02 as (
  select * from {{ ref('S_RECIPIENT_S_VEEAM_R_HR_RECIPIENT_U_VEEAM_SRC_F_ACCOUNT') }} 
),

bo_set as (
  select

  query_00.RECIPIENT_BK as 'BK_RECIPIENT',
  query_00.RECIPIENT_H as 'HK_RECIPIENT',

  query_01.TIER1 as 'TIER1',
  query_01.TIER2 as 'TIER2',
  query_01.COUNTRY as 'COUNTRY',

  query_02.ACCOUNT_TYPE as 'ACCOUNT_TYPE',
  query_02.SEGMENT as 'SEGMENT'
  from 
   query_00 
    left join  query_02  on query_02.RECIPIENT_H = query_00.RECIPIENT_H
    left join  query_01  on query_01.RECIPIENT_H = query_00.RECIPIENT_H
  )

select * from bo_set
{%else%}
select * from
{{ source('dv','RECIPIENT_S_DVB_HUB') }}
{% endif %}
