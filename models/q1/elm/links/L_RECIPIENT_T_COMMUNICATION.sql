
with 
hr_communication as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['LEAD_ID','ACTIVITY_ID']) }} as recipient_t_communication_h,
      
    {{ dbt_utils.generate_surrogate_key(['LEAD_ID']) }} as recipient_h,
    LEAD_ID as recipient_LEAD_ID,
      
    {{ dbt_utils.generate_surrogate_key(['ACTIVITY_ID']) }} as communication_h,
    ACTIVITY_ID as communication_ACTIVITY_ID
      
    from {{ ref('hr_communication') }} 
)

select * from hr_communication
