
with 
hr_recipient as (
  select  
    {{ dbt_utils.generate_surrogate_key(['ID'])}} as recipient_h,
    {{ dbt_utils.generate_surrogate_key(['TIER1', 'TIER2', 'COUNTRY'])}} as hkdiff,
    TIER1,
    TIER2,
    COUNTRY
  from {{ ref('hr_recipient') }} 
)
select * from hr_recipient
