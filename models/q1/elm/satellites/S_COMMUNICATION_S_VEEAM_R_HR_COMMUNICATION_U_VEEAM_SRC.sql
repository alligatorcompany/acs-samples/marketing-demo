
with 
hr_communication as (
  select  
    {{ dbt_utils.generate_surrogate_key(['ACTIVITY_ID'])}} as communication_h,
    {{ dbt_utils.generate_surrogate_key(['LEAD_ID', 'ACTIVITY_DATE', 'PRIMARY_ATTRIBUTE_VALUE_ID', 'ACTIVITY_TYPE_ID', 'NAME', 'SENT', 'DELIVERED', 'OPENED', 'CKLICKED', 'BOUNCE', 'UNSUBSCRIBE', 'BOUNCE_SOFT'])}} as hkdiff,
    ACTIVITY_ID,
    LEAD_ID,
    ACTIVITY_DATE,
    PRIMARY_ATTRIBUTE_VALUE_ID,
    ACTIVITY_TYPE_ID,
    NAME,
    SENT,
    DELIVERED,
    OPENED,
    CKLICKED,
    BOUNCE,
    UNSUBSCRIBE,
    BOUNCE_SOFT
  from {{ ref('hr_communication') }} 
)
select * from hr_communication
