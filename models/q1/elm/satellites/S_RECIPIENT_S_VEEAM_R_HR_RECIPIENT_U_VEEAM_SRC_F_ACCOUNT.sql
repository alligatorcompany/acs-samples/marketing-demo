
with 
hr_recipient as (
  select  
    {{ dbt_utils.generate_surrogate_key(['ID'])}} as recipient_h,
    {{ dbt_utils.generate_surrogate_key(['ACCOUNT_TYPE', 'SEGMENT'])}} as hkdiff,
    ACCOUNT_TYPE,
    SEGMENT
  from {{ ref('hr_recipient') }} 
)
select * from hr_recipient
