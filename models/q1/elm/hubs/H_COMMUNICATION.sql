
with 
hr_communication as (
  select  
    {{ dbt.concat(['ACTIVITY_ID']) }} as communication_bk,
    {{ dbt_utils.generate_surrogate_key(['ACTIVITY_ID']) }} as communication_h
  from {{ ref('hr_communication') }} 
)

select * from hr_communication

