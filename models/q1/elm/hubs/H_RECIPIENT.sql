
with 
hr_recipient as (
  select  
    {{ dbt.concat(['ID']) }} as recipient_bk,
    {{ dbt_utils.generate_surrogate_key(['ID']) }} as recipient_h
  from {{ ref('hr_recipient') }} 
),
hr_communication as (
  select  distinct
    {{ dbt.concat(['LEAD_ID']) }} as recipient_bk,
    {{ dbt_utils.generate_surrogate_key(['LEAD_ID']) }} as recipient_h
  from {{ ref('hr_communication') }} 
)

select * from hr_recipient
 union 
select * from hr_communication

