with

contact as (
    select
        {{ dbt_utils.star(source('csv', 'veeam_contact')) }}
    from {{ source('csv', 'veeam_contact') }}
),

inquiry as (
    select
        {{ dbt_utils.star(source('csv', 'veeam_inquiry')) }}
    from {{ source('csv', 'veeam_inquiry') }}
)

select * FROM  contact
union
select * from inquiry
