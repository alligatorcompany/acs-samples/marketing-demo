with

marketingactivity_type as (
    select
        {{ dbt_utils.star(source('csv', 'veeam_marketingactivity_type')) }}
    from {{ source('csv', 'veeam_marketingactivity_type') }}
),

marketingactivity as (
    select
        {{ dbt_utils.star(source('csv', 'veeam_marketingactivity')) }}
    from {{ source('csv', 'veeam_marketingactivity') }}

),

activity_name as (
    select
        marketingactivity.*,
        marketingactivity_type.name
    from marketingactivity
    left outer join
        marketingactivity_type
        on marketingactivity_type.id = marketingactivity.activity_type_id

)

select
    activity_name.*,
    coalesce(activity_name.name = 'Send Email', false) as sent,
    coalesce(activity_name.name = 'Email Delivered', false) as delivered,
    coalesce(activity_name.name = 'Open Email', false) as opened,
    coalesce(activity_name.name = 'Click Email', false) as cklicked,
    coalesce(activity_name.name = 'Email Bounced', false) as bounce,
    coalesce(activity_name.name = 'Unsubscribe Email', false) as unsubscribe,
    coalesce(activity_name.name = 'Email Bounced Soft', false) as bounce_soft

from activity_name
