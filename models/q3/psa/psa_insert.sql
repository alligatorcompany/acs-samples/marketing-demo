{{
    config(
        materialized='incremental'
    )
}}

with

maxldts as (

    select
        source_name,
        interface_schema,
        interface_name,
        max(ldts) as maxldts

    from {{ source('psa', 'psa_landing') }}
    group by 1, 2, 3

),

landing as (
    select * from {{ source('psa', 'psa_landing') }}
),

psa_landing as (

    select
        landing.ldts,
        landing.source_name,
        landing.interface_schema,
        landing.interface_name,
        landing.id_cols,
        landing.bk,
        landing.payload_hash,
        landing.payload

    from landing
    join maxldts on maxldts.source_name = landing.source_name
        and maxldts.source_name = landing.source_name
        and maxldts.interface_schema = landing.interface_schema
        and maxldts.interface_name = landing.interface_name
        and maxldts.maxldts = landing.ldts

{% if is_incremental() %}
),

psa_latest as (

    select
        ldts,
        source_name,
        interface_schema,
        interface_name,
        id_cols,
        bk,
        payload_hash,
        payload

    from (
        select
            ldts,
            source_name,
            interface_schema,
            interface_name,
            id_cols,
            bk,
            payload_hash,
            payload,
            row_number() over (
                partition by
                    source_name,
                    interface_schema,
                    interface_name,
                    bk order by ldts desc) as ranked
        from
            {{ this }}
    )
    where ranked = 1
{% endif %}
)

select

    psa_landing.ldts,
    psa_landing.source_name,
    psa_landing.interface_schema,
    psa_landing.interface_name,
    psa_landing.id_cols,
    psa_landing.bk,
    psa_landing.payload_hash,
    psa_landing.payload

from psa_landing

{% if is_incremental() %}

    left join psa_latest target on target.source_name=psa_landing.source_name
    and target.interface_schema=psa_landing.interface_schema
    and target.interface_name=psa_landing.interface_name
    and target.id_cols=psa_landing.id_cols
    and target.bk=psa_landing.bk
    and target.payload_hash=psa_landing.payload_hash
    where target.payload_hash is null


{% endif %}